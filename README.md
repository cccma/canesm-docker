# Docker setup for CanESM

Create a docker image with required libs to run CanESM

## Contents

- Based on Ubuntu:18.04
- Adds gcc, gfortran, netcdf, openmpi, git, emacs
- Adds canesm user
- Clones in CanESM source code

## Some commands for building and running

docker build -f Dockerfile.esmf -t swartn/esmf .
docker build -f Dockerfile.canesm -t swartn/canesm-docker .

docker run -it swartn/canesm-docker

docker image prune
docker container prune
